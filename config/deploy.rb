require 'bundler/capistrano'
require 'rvm/capistrano'
require 'delayed/recipes'

set :rvm_ruby_string, :local
set :rvm_autolibs_flag, "read-only"

before "deploy:setup", 'rvm:install_rvm'
before 'deploy:setup', 'rvm:install_ruby'
before 'deploy:setup', 'rvm:create_gemset'

set :application, "faxriot"
set :repository,  "git@bitbucket.org:madgeekfiend/faxriot.git"
set :branch, "master"
set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

server "198.199.119.238", :web, :app
role :db, "198.199.119.238", :primary => true

set :user, 'totalgeek'
set :password, 'sm78con'
set :use_sudo, false
set :deploy_to, '/home/totalgeek/www'
set :ssh_options, { :forward_agent => true }
set :deploy_via, :remote_cache

set :rails_env, "production"

# if you want to clean up old releases on each deploy uncomment this:
#after "deploy:restart", "deploy:cleanup"
before "deploy:restart", "deploy:migrate"
before "deploy:cold", "deploy:install_bundler"
after "deploy:stop", "delayed_job:stop"
after "deploy:start", "delayed_job:start"
after "deploy:restart", "delayed_job:restart"

default_run_options[:pty] = true

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
     run "ln -nsf #{shared_path}/faxes #{current_path}/faxes"
   end

   task :install_bundler, :roles => :app do
     run "type -P bundle &>/dev/null || { gem install bundler --no-rdoc --no-ri; }"
   end

   namespace :assets do
     task :precompile, :roles => :web, :except => { :no_release => true } do
       from = source.next_revision(current_revision) rescue nil

       if from.nil? || capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
         run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
       else
         logger.info "Skipping asset pre-compilation because there were no asset changes"
       end
     end
   end
end

