require 'sr_fax_client'

class FaxController < ApplicationController
  include SimpleCaptcha::ControllerHelpers
  include FaxHelper

  def cover_page
    @fax = Fax.find_by_uniq_id params[:uniq_id]
    @optional_text = @fax.cover_text || 'None'
    render :layout => false
  end

  def create
    if simple_captcha_valid? or (Rails.env.development? or Rails.env.test?)
      fax = Fax.new(params[:fax])
      # Now log the user info
      fax.remote_addr=request.env['REMOTE_ADDR']
      fax.http_referrer=request.env['HTTP_REFERER']
      fax.http_user_agent=request.env['HTTP_USER_AGENT']
      #check the user hasn't sent more than 3 faxes the last 24 hours
      user_check = Fax.select('sanitized_email').where('created_at > ? AND sanitized_email = ?', Time.now - 24.hours, normalize_email(fax.from_email))
      if user_check.count < 3
        if fax.save
          redirect_to pending_fax_path(:uniq_id => fax.uniq_id)
        else
          # Validation problems tell the user to go back
          flash[:error] = join_errors_html(fax.errors.full_messages) if fax.errors.present?
        end
      else
        flash[:error] = 'You can only submit 3 free faxes in a 24 hour time period. Please try again later.'
        redirect_to root_path
      end
    else
      flash[:error]='Captcha was entered incorrectly.'
      redirect_to root_url
    end
  end

  def pending_fax
    @fax = Fax.find_by_uniq_id(params[:uniq_id])
    raise ActionController::RoutingError.new('Web page not found.') if @fax.blank?
    SRFaxClient.delay.create_fax_cover_sheet(@fax.uniq_id,cover_page_url(:uniq_id => @fax.uniq_id))
  end

  def approve_fax
    if Rails.env.development? and params[:uniq_id] == 'dev'
      @fax = Fax.first || Fax.new
      @fax.from_email='test@test.com'
    else
      @fax = Fax.find_by_uniq_id(params[:uniq_id])
      if @fax.confirm_token == params[:confirm_token] and not @fax.is_confirmed
        @fax.is_confirmed=true
        @fax.save!
        unless Rails.env.test?
          if Rails.env.development?
            SRFaxClient.send_fax(@fax)
          else
            SRFaxClient.delay.send_fax(@fax)
          end
        end
      else
        if @fax.confirm_token != params[:confirm_token]
          redirect_to root_url, :flash =>{ :error => 'The security token in this link is unknown. Make sure you are clicking on the approval link found in your email.'}
        else
          redirect_to root_url, :flash =>{ :error => 'This fax has already been confirmed. Check your email for status.'} if not @fax.is_sent?
          redirect_to root_url, :flash=>{ :error=>'This fax has already been delivered.'} if @fax.is_sent?
        end
      end
    end
  end

end
