class HomeController < ApplicationController

  def index
    @fax = Fax.new
    3.times { @fax.attachments.build }
  end
end
