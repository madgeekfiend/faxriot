class FaxMailer < ActionMailer::Base
  default from: "Fax Riot <no-reply@faxriot.com>"

  def approve_fax_mail(fax)
    @fax = fax
    Rails.logger.info(@fax.inspect)
    mail( :to => fax.from_email, :subject =>'[Fax Riot] Click the link to approve your fax.' )
  end

  def success_fax(fax, message = nil)
    @fax = fax
    @success_message = JSON.pretty_generate(message) unless message.blank?
    mail( :to => fax.from_email, :subject =>'[Fax Riot] Your fax has been successfully sent.')
  end

  def error_fax(fax, error_message = nil)
    @fax = fax
    @error_message = JSON.pretty_generate(error_message) unless error_message.blank?
    mail( :to =>fax.from_email, :subject => '[Fax Riot] There was an error in sending your fax.')
  end
end
