module FaxHelper

  #Sanitize the email address so anything that looks like
  # sam.contapay+100@gmail.com looks like
  # samcontapay@gmail.com
  def normalize_email(email)
    tokens = email.split('@')
    first_part = tokens[0].gsub('.','')

    first_part = first_part[0..first_part.index('+')-1] if first_part.include?'+'
    [first_part,tokens[1]].join('@')
  end

  def join_errors_html errors
    #result = ''
    #errors.each { |msg| result << "<p>#{msg}</p>"}
    test = errors.map {|msg| "<p>#{msg}</p>"}
    test.join('')
  end
end
