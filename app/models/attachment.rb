require 'file_size_validator'

class Attachment < ActiveRecord::Base
  attr_accessible :fax_id, :upload
  belongs_to :fax
  mount_uploader :upload, AttachmentUploader
  validates :upload,
    :file_size => {
      :maximum => 5.megabytes.to_i
    }
  before_create :generate_token

  def base64encoded
    Base64.encode64(open(upload.current_path){ |io| io.read })
  end

  protected

  def generate_token
    self.unique_token = loop do
      random_token = SecureRandom.urlsafe_base64
      break random_token unless Attachment.where(unique_token: random_token).exists?
    end
  end

end
