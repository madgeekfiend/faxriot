class Faq < ActiveRecord::Base
  attr_accessible :answer, :order, :question, :active
  validates_presence_of :answer
  validates_presence_of :question

end
