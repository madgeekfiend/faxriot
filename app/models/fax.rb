class Fax < ActiveRecord::Base
  include FaxHelper
  apply_simple_captcha
  attr_accessible :cover_text, :from_company, :from_email, :from_name, :to_company, :to_fax, :to_name, :attachments_attributes
  has_many :attachments, :dependent => :destroy
  accepts_nested_attributes_for :attachments
  before_create :generate_unique
  before_create :clean_email

  validates_presence_of :from_name
  validates_presence_of :from_email
  validates_presence_of :to_name
  validates :to_fax, :presence => true, :numericality => true
  validates_length_of :to_fax, :is => 10

  private

  def clean_email
    self.sanitized_email= normalize_email(self.from_email)
  end

  def generate_unique
    self.uniq_id = Digest::SHA2.hexdigest(Time.now.to_s + self.from_name + self.from_email)[-10,10]
    self.confirm_token= Digest::SHA2.hexdigest(self.from_name+self.from_email+Time.now.to_s)[-12,12]
  end

end
