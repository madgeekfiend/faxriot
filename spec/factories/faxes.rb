FactoryGirl.define do

  factory :fax do
    cover_text 'cover'
    from_company Faker::Company.name
    sequence(:from_email) {|n| "sam.contapay+100#{n}@gmail.com"}
    from_name Faker::Name.name
    to_company Faker::Company.name
    to_fax "5551234433"
    to_name Faker::Name.name
  end

end
