FactoryGirl.define do
  factory :faq do
    question 'Who am I?'
    answer Faker::Lorem.paragraph(4)
  end
end


