# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    title "This is a title"
    order 1
    body Faker::Lorem.paragraphs 2
    active false
  end
end
