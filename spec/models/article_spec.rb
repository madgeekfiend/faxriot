require 'spec_helper'

describe Article do
  it 'should create a new article' do
    expect(FactoryGirl.build(:article)).to be_valid
  end
  it 'should save a new article' do
    expect(FactoryGirl.create(:article)).to be_valid
  end
end
