require 'spec_helper'

describe Fax do
  it 'creates has a valid model' do
    expect(FactoryGirl.build(:fax)).to be_valid
  end
  it 'generates uniq_id and confirm token before save' do
    fax = FactoryGirl.build(:fax)
    fax.uniq_id.present?
    fax.confirm_token.present?
  end
  it' when saved the email sam.contapay@gmail.com equals samcontapay@gmail.com' do
    expect(FactoryGirl.create(:fax, from_email: 'sam.contapay@gmail.com').sanitized_email).to eq 'samcontapay@gmail.com'
  end
  it 'when saved the email sam.contapay+1000@gmail.com equals samcontapay@gmail.com' do
    expect(FactoryGirl.create(:fax, from_email: 'sam.contapay+1000@gmail.com').sanitized_email).to eq 'samcontapay@gmail.com'
  end
  it 'throws an error if from_name is missing' do
    expect(FactoryGirl.build(:fax, from_name: nil)).to have(1).errors_on(:from_name)
  end
  it 'throws an error if from_email is missing' do
    expect(FactoryGirl.build(:fax, from_email: nil)).to have(1).errors_on(:from_email)
  end
  it 'throws an error if to_name is missing' do
    expect(FactoryGirl.build(:fax, to_name: nil)).to have(1).errors_on(:to_name)
  end
  it 'throws an error if to_fax is missing' do
    expect(FactoryGirl.build(:fax, to_fax: nil)).to_not be_valid
  end
  it 'is not valid when you enter more then 10 digits for the phone number' do
    expect(FactoryGirl.build(:fax, to_fax: 34445432123)).to have(1).errors_on(:to_fax)
  end

  it {should_not allow_mass_assignment_of(:remote_addr)}
  it {should_not allow_mass_assignment_of(:http_referrer)}
  it {should_not allow_mass_assignment_of(:http_user_agent)}
end
