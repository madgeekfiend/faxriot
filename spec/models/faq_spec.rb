require 'spec_helper'

describe Faq do
  it 'valid with answer and question' do
    expect(FactoryGirl.build(:faq)).to be_valid
  end
  it 'is invalid if answer is missing' do
    expect(FactoryGirl.build(:faq, answer: nil)).to have(1).errors_on(:answer)
  end
  it 'is invalid if question is missing' do
    expect(FactoryGirl.build(:faq, question: nil)).to have(1).errors_on(:question)
  end
end