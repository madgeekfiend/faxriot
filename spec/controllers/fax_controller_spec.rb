require 'spec_helper'

describe FaxController do
  Delayed::Worker.delay_jobs = false

  describe "#create" do
    it 'if there are 4 fax records with the same email it should fail with flash error and redirect to root' do
      3.times { |x| FactoryGirl.create(:fax, from_email: 'same@email.com') }
      post :create, fax: FactoryGirl.attributes_for(:fax, from_email: 'same@email.com')
      should set_the_flash[:error].to('You can only submit 3 free faxes in a 24 hour time period. Please try again later.')
      response.should redirect_to root_url
    end
    it 'if there are less than 3 fax records with the same email in the last 24 hours it should succeed' do
      2.times { |x| FactoryGirl.create(:fax, from_email: 'same@email.com') }
      thefax = FactoryGirl.attributes_for(:fax, from_email: 'same@email.com')
      post :create, fax: thefax
      flash[:error].should be_nil
      expect(response.status).to eq(302)
    end
    it 'should send send fax if 3 emails are in the system already' do
      10.times { |x| FactoryGirl.create(:fax, from_email: Faker::Internet.email) }
      post :create, fax: FactoryGirl.attributes_for(:fax, from_email: 'random@email.com')
      expect(flash[:error]).to be_nil
      expect(response.status).to eq(302)
    end
  end
  it 'should render pending fax template when going to pending fax path' do
    SRFaxClient.stub(:delay)
    @mock_delay = double('mock_delay').as_null_object
    SRFaxClient.stub(:delay).and_return(@mock_delay)

    fax = FactoryGirl.create(:fax)
    get :pending_fax, uniq_id: fax.uniq_id
    response.should render_template 'fax/pending_fax'
  end
  it 'should throw routing error when it can not find a pending fax' do
    expect{get(:pending_fax, uniq_id: 'testtest')}.to raise_error ActionController::RoutingError
  end

  describe 'on approve fax' do
    let(:fax) { FactoryGirl.create(:fax)}

    it 'should render template if passed a valid fax uniq ID and update is_confirmed to true' do
      get(:approve_fax, {:confirm_token=>fax.confirm_token,:uniq_id=>fax.uniq_id})
      expect(response).to render_template 'fax/approve_fax'
    end
    it 'given the wrong confirm token' do
      get(:approve_fax, {:confirm_token=>'WRONG_CONFIRM_TOKEN',:uniq_id=>fax.uniq_id})

      should set_the_flash[:error].to('The security token in this link is unknown. Make sure you are clicking on the approval link found in your email.')
      should redirect_to(root_url)
    end
    it 'given the wrong confirm token and fax has been sent' do
      fax = FactoryGirl.create(:fax, {is_confirmed: true, is_sent: false})
      get(:approve_fax, {:confirm_token=>fax.confirm_token,:uniq_id=>fax.uniq_id})
      should set_the_flash[:error].to('This fax has already been confirmed. Check your email for status.')
    end
  end

end
