require 'spec_helper'

describe ArticleController do
 it 'should list all Articles when going to index' do
   article=FactoryGirl.create(:article)
   get :index
   expect(assigns(:articles)).to eq([article])
 end
end
