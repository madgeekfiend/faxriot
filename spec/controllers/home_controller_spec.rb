require 'spec_helper'

describe HomeController do
  describe "GET #index" do
    it 'on success responds with an HTTP 200 status code' do
      get :index
      expect(response).to be_success
    end

  end
end
