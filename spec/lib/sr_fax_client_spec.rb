require 'spec_helper'

describe 'SRFaxClient' do
  Delayed::Worker.delay_jobs = false


  describe '#send_fax' do
    before(:each) do
      SRFaxClient.stub(:delay)
      @mock_delay = double('mock_delay').as_null_object
      SRFaxClient.stub(:delay).and_return(@mock_delay)
      SRFaxClient.stub(:base64_encode_cover_sheet)
    end

    it 'should send email on success' do
      client = double("client")
      client.stub(:call).and_return({:queue_fax_response => {:status=>'Success',:result=>'FASDFSDFSD'}})
      SRFaxClient.stub(:get_sr_client).and_return(client)
      SRFaxClient.should_receive(:base64_encode_cover_sheet)
      fax = FactoryGirl.create(:fax)
      @mock_delay.should_receive(:get_fax_status).with(fax)
      SRFaxClient.send_fax(fax)
    end

    it 'should send error email on fail' do
      client = double("client")
      client.stub(:call).and_return({:queue_fax_response => {:status=>'Fail',:result=>'FASDFSDFSD'}})
      SRFaxClient.stub(:get_sr_client).and_return(client)
      FaxMailer.stub(:delay).and_return(@mock_delay)
      SRFaxClient.should_receive(:base64_encode_cover_sheet)
      fax = FactoryGirl.create(:fax)
      @mock_delay.should_receive(:error_fax).with(fax)

      SRFaxClient.send_fax(fax)
    end
  end

  describe '#get_fax_status' do
    before(:each) do
      SRFaxClient.stub(:delay)
      @mock_delay = double('mock_delay').as_null_object
      SRFaxClient.stub(:delay).and_return(@mock_delay)
    end

    it 'should raise an error when internet fax id is empty' do
      fax = FactoryGirl.create(:fax, internet_fax_id: nil)
      lambda{SRFaxClient.get_fax_status(fax)}.should raise_error(RuntimeError)
    end

    it 'will send a success fax email on success if status is Success' do
      client = double("client")
      client.stub(:call).and_return({:get_fax_status_response => {:status=>'Success',:result=>{:item=>'something'}}})
      SRFaxClient.stub(:get_sr_client).and_return(client)
      fax = FactoryGirl.create(:fax,internet_fax_id: 'FAXID')
      FaxMailer.stub(:delay).and_return(@mock_delay)
      @mock_delay.should_receive(:success_fax)
      SRFaxClient.get_fax_status(fax)
    end

    it 'will send an error message if status is not Success' do
      client = double("client")
      client.stub(:call).and_return({:get_fax_status_response => {:status=>'Failure',:result=>{:item=>'something'}}})
      SRFaxClient.stub(:get_sr_client).and_return(client)
      fax = FactoryGirl.create(:fax,internet_fax_id: 'FAXID')
      FaxMailer.stub(:delay).and_return(@mock_delay)
      @mock_delay.should_receive(:error_fax)
      SRFaxClient.get_fax_status(fax)
    end

  end
end