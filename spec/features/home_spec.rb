require 'spec_helper'

feature 'home page' do

  scenario 'welcomes user' do
    visit root_url
    page.should have_content('Send a Fax')
  end

  scenario 'should have a begin button if ALLOW_FAX=true' do
    ALLOW_FAX=true
    visit root_url
    page.should have_selector( :link_or_button, 'Begin')
  end

  scenario 'should not have a begin button if ALLOW_FAX=false' do
    ALLOW_FAX=false
    visit root_url
    page.should_not have_selector(:link_or_button, 'Begin')
  end

  scenario 'should have a home link' do
    visit root_url
    page.should have_selector(:link_or_button, 'Home')
  end

  scenario 'should have a Faq link' do
    visit root_url
    page.should have_selector(:link_or_button, 'FAQ')
  end

end