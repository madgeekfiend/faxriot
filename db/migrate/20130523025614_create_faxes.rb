class CreateFaxes < ActiveRecord::Migration
  def change
    create_table :faxes do |t|
      t.string :from_name
      t.string :from_company
      t.string :from_email
      t.string :to_name
      t.string :to_company
      t.string :to_fax
      t.string :uniq_id
      t.text :cover_text
      t.string :confirm_token, :null=>false
      t.boolean :is_confirmed, :default=>false
      t.boolean :is_sent, :default=>false
      t.string :internet_fax_id

      t.timestamps
    end
    add_index :faxes, :confirm_token
    add_index :faxes, :uniq_id
    add_index :faxes, :internet_fax_id
    add_index :faxes, :from_email
  end
end
