class AddFieldsToFax < ActiveRecord::Migration
  def change
    add_column :faxes, :remote_addr, :string
    add_column :faxes, :http_referrer, :string
    add_column :faxes, :http_user_agent, :text

    add_index :faxes, :remote_addr
  end
end
