class AlterToFaxonFax < ActiveRecord::Migration
  def up
    change_column :faxes, :to_fax, :string
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
