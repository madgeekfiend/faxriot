class AddFieldFaxes < ActiveRecord::Migration
  def change
    add_column :faxes, :is_queued, :boolean, :default => false
    add_column :faxes, :queued_at, :timestamp
  end
end
