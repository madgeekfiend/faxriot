class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.integer :fax_id
      t.string :upload
      t.timestamps
    end
  end
end
