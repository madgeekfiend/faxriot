class FaxAddFieldSanitizeEmail < ActiveRecord::Migration
  def change
    add_column :faxes, :sanitized_email, :string

    add_index :faxes, :sanitized_email
  end
end
