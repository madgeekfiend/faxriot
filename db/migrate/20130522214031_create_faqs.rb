class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.text :question
      t.text :answer
      t.integer :order, :default => 100
      t.boolean :active, :default => true
      t.timestamps
    end

  end
end
