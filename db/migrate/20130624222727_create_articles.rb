class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.integer :order
      t.text :body
      t.boolean :active

      t.timestamps
    end
  end
end
