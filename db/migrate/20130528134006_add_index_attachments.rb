class AddIndexAttachments < ActiveRecord::Migration
  def change
    add_index :attachments, :unique_token
    add_index :attachments, :original_filename
  end
end
