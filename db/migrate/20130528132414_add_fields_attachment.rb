class AddFieldsAttachment < ActiveRecord::Migration
  def change
    change_table :attachments do |t|
      t.string :unique_token
      t.string :original_filename
    end
  end
end
