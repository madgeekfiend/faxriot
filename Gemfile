source 'https://rubygems.org'

gem 'rails', '3.2.13'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'
gem 'haml'
gem 'carrierwave'
gem 'fog', '~> 1.3.1'
gem 'savon'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'simple_captcha', :git => 'git://github.com/galetahub/simple-captcha.git'
gem 'devise'
gem 'rails_admin'
gem 'zipruby'
gem 'pdfkit'
gem 'newrelic_rpm'

gem 'simplecov', :require => false, :group => :test

group :development do
  gem 'sqlite3'
  gem 'awesome_print'
  gem 'better_errors'
  gem 'binding_of_caller'
end

group :development, :test do
  gem 'faker'
  gem 'factory_girl_rails'
  gem 'rspec-rails', '~> 2.0'
  gem 'pry'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'shoulda-matchers'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

gem 'jquery-rails'

group :production do
  gem 'pg'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
gem 'capistrano'
gem 'rvm-capistrano'

# To use debugger
# gem 'debugger'
