class SRFaxClient

  def self.send_fax(fax)
    client = self.get_sr_client

    #Build fax hash send with
    message = {
      access_id: SRFAX_USER_NUMBER,
      access_pwd: SRFAX_USER_PASSWORD,
      sCallerID: FAX_NUMBER,
      sSenderEmail: fax.from_email,
      sFaxType: "SINGLE",
      sToFaxNumber: '1' + fax.to_fax,
      sAccountCode: "",
    }

    #Add cover sheet
    message["sFileName_1"]='cover_sheet.pdf'
    message["sFileContent_1"]=self.base64_encode_cover_sheet(fax.uniq_id) #Base64.strict_encode64(File.binread("#{Rails.root}/faxes/cover_pages/#{fax.uniq_id}.pdf"))

    fax.attachments.each_with_index do |value, index|
      message["sFileName_#{index+2}"]=value.original_filename
      message["sFileContent_#{index+2}"]= Base64.strict_encode64(value.upload.read)
    end

    response = client.call(:queue_fax, message: message)
    response = response.to_hash

    if response[:queue_fax_response][:status] == 'Success'
      fax.is_queued=true
      fax.queued_at=Time.now
      fax.internet_fax_id = response[:queue_fax_response][:result]
      fax.attachments.destroy_all unless Rails.env.development?
      fax.save!

      if Rails.env.development?
        FaxMailer.success_fax(fax).deliver
        self.get_fax_status(fax)
      else
        FaxMailer.delay.success_fax(fax)
        self.delay.get_fax_status(fax)
      end
      true
    else
      if Rails.env.development?
        FaxMailer.error_fax(fax).deliver
      else
        FaxMailer.delay.error_fax(fax)
      end
      false
    end
  end


  def self.get_fax_status fax
    raise "No Internet Fax ID Present" if fax.internet_fax_id.blank?
    client = self.get_sr_client
    message = {
      access_id: SRFAX_USER_NUMBER,
      access_pwd: SRFAX_USER_PASSWORD,
      sFaxDetailID: fax.internet_fax_id
    }

    status = client.call(:get_fax_status, message: message).to_hash

    if status[:get_fax_status_response][:status] == 'Success'
      # We are good to go
      # Send user a success fax email
      fax.is_sent = true
      fax.save!
      if Rails.env.development?
        FaxMailer.success_fax(fax,status[:get_fax_status_response][:result][:item]).deliver
      else
        FaxMailer.delay.success_fax(fax,status[:get_fax_status_response][:result][:item])
      end
    else
      #Problem send error message
      if Rails.env.development?
        FaxMailer.error_fax(fax,status[:get_fax_status_response][:result][:item]).deliver
      else
        FaxMailer.delay.error_fax(fax,status[:get_fax_status_response][:result][:item])
      end
    end
  end

  def self.create_fax_cover_sheet(uniq_id,cover_page_url)
    Dir.mkdir("#{Rails.root}/faxes/cover_pages") unless File.exists?("#{Rails.root}/faxes/cover_pages")
    kit = PDFKit.new(cover_page_url, :page_size=>'Letter')
    kit.to_file("#{Rails.root}/faxes/cover_pages/#{uniq_id}.pdf")
    FaxMailer.delay.approve_fax_mail(Fax.find_by_uniq_id(uniq_id))
  end

  private

  def self.base64_encode_cover_sheet(uniq_id)
    Base64.strict_encode64(File.binread("#{Rails.root}/faxes/cover_pages/#{uniq_id}.pdf"))
  end

  def self.get_sr_client
    Savon.client(wsdl: 'https://www.srfax.com/SRF_UserFaxWebSrv_doc.php?wsdl')
  end

end